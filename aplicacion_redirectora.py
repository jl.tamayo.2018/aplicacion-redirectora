#!/usr/bin/python3

import socket
import random

URLs = ['https://www.urjc.es/',
        'https://www.instagram.com/',
        'https://www.minecraft.net/es-es',
        'https://www.google.com',
        'https://www.amazon.es/']

if __name__ == "__main__":
    mySocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    # para arreglar lo del puerto ocupado
    mySocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

    mySocket.bind(('', 1234))

    # cola máxima de 5 conexiones TCP
    mySocket.listen(5)

    try:
        while True:
            print("Esperando alguna conexion...")
            connectionSocket, addr = mySocket.accept()
            print("Conexion recibida de: " + str(addr))
            recibido = connectionSocket.recv(2048)
            print(recibido)

            url_aleatoria = random.choice(URLs)

            respuesta = "HTTP/1.1 307 Temporary Redirect\r\n" \
                        + "Location: " + str(url_aleatoria) \
                        + "\r\n"

            # codificar a utf8 para poder enviar el mensaje
            connectionSocket.send(respuesta.encode('utf-8'))
            connectionSocket.close()
    except KeyboardInterrupt:
        print("Cerrando servidor", end='')
        mySocket.close()
